/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>

int main(void)
{
	printf("Hello World! System has booted and it is running. To stop the emulation, press Ctrl+A+X.");
	return 0;
}
